## 关于本项目
- spring boot 2.0 后,加强了oauth2的支持。研究源码后，主要是增加了 http.oauth2Login() 。
- 本项目 已更新 到 2.3 ，适配 Spring Boot 2.3.2 。
- 本项目是 Spring Security OAuth2 Login 的二次封装，目的在于定制 Spring Security OAuth2的配置，以适配中国国内oauth2提供商的规范（微信、QQ、钉钉、码云）以及我单位的中央认证的支持，使大家在使用 Spring Security 框架进行 OAuth2 认证授权时，使用原生的配置属性即可，省去大量踩坑与调试的时间。
- 本项目后续会增加 国内其它支持OAuth2第三方登陆厂商 的自动配置功能，如：钉钉小程序、钉钉H5微应用等。
- 本项目已发布到Maven中央仓库，https://search.maven.org/artifact/com.gitee.dgut-sai/security-oauth2 。

## 演示Demo
使用本项目的演示Demo，请参考这个仓库：https://gitee.com/dgut-sai/security-oauth2-sample

## 版本更新
- 2.3 版本，增加 微信公众号平台 微信网页授权登录。针对的是用户在 微信客户端 中访问第三方网页，公众号可以通过微信网页授权机制，来获取用户基本信息，进而实现业务逻辑。测试时，网页需要在 微信开发者工具中 打开。
  - 微信开发者工具下载：https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html
  - 微信公众平台：https://mp.weixin.qq.com/
- 2.2.1版本，优化代码。
- 2.2版本，增加 码云 OAuth2 登录 ; 重构代码。
- 2.1版本，增加 钉钉扫码登录独立第三方应用。注意：此功能与企业自建应用/第三方企业应用无关，只能用扫码登录打开第三方网站，并且不是钉钉内的应用免登，此流程只能做到获取到用户身份（无手机号和企业相关信息）。

## 编译项目
mvn install

## 如何使用本项目
#### 本项目已发布到 Maven中央仓库，可以直接添加依赖使用。
#### 新建一个 spring boot 项目，在 pom.xml 中加入:

        <dependency>
            <groupId>com.gitee.dgut-sai</groupId>
            <artifactId>security-oauth2</artifactId>
            <version>2.2.1</version>
        </dependency>
   
#### 还有一些Web项目必要的模块：

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

#### 修改 application.yml，配置各oauth2提供商的 appid 和 appsecret，其它的信息不用填。

~~~
spring:
  security:
    oauth2:
      client:
        registration:
          github:
            client-id: *****
            client-secret: *****
          weixin:
            client-id: *****
            client-secret: ******
          dgut:
            client-id: *****
            client-secret: *****
          qq:
            client-id: *****
            client-secret: *****
          # 进入 钉钉开发者平台 的页面，点击左侧菜单的【移动接入应用-登录】，然后点击右上角的【创建扫码登录应用授权】，创建用于免登过程中验证身份的appId及appSecret，创建后即可看到appId和appSecret。          
          dingding:
            client-id: *****
            client-secret: *****
          # v2.2 增加码云 oauth2 登录
          gitee:
            client-id: ****
            client-secret: ****
          # 微信公众号平台 微信网页授权登录 , 可以在公众号管理后台申请测试号，以获取appid与secret。
          # 测试时，网页需要在 微信开发者工具中 打开。
          WXMP:
            client-id: ****
            client-secret: ****

# 下面的属性设置获取code后的回调地址的前缀，最终根据注册ID解释后的地址如：/uaa/login/qq、/uaa/login/weixin            
# 默认值是：/login/oauth2/code，最终值为：/login/oauth2/code/qq、/login/oauth2/code/weixin
# 把最终的url设置在OAuth2提供商的回调地址那, 如：http://localhost/uaa/login/weixin
sai:
  security:
    oauth2:
      authorizationResponseBasePath: /uaa/login
~~~        
#### 回调域名 会自动识别
- 如果你的域名是 www.dgut.edu.cn ， 则回调地址会自动识别为： http://www.dgut.edu.cn/uaa/login/** ，**代表是qq、weixin、dingding 或 dgut 等；
- https 也会自动识别；
- 把最终识别的 url 设置到 OAuth2第三方登陆提供商的后台设置。
#### 最后，在 spring security 中添加配置：
~~~java
@EnableSaiOAuth2Login
@Configuration
public class SaiOAuth2LoginConfiguration extends WebSecurityConfigurerAdapter {

    // 设置security的拦截规则 START
    @Override
    public void configure(WebSecurity web) {
        // 不能在这个方法里忽略登录login的路径
        // 否则security的filter都不匹配login路径造成不能登录
        // 在这里忽略掉静态资源的路径是比较好的选择
        // 这与在配置文件设置security.ignoring是一样的
        web.ignoring().antMatchers("/css/**", "/js/**", "/fonts/**", "/images/**", "/favicon.ico", "/webjars/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http
                .authorizeRequests()
                    .anyRequest().authenticated()
                    .and()
                .apply(new SaiOAuth2LoginSecurityConfigurer())
                    .and()
                .oauth2Login().loginPage("/login").permitAll()
//                                .defaultSuccessUrl("/",true)
                                .successHandler((request, response, authentication) -> {
                                    // 一般登录成功后，我们会根据业务要求作一些处理。我们可以定义successHandler完成这个需求。
                                    // 当我们定义了 successHandler 后，defaultSuccessUrl会失效。
                                    // 注意，authentication 变量的实际类型是 OAuth2AuthenticationToken 。
                                    // 同时，SecurityContextHolder.getContext().getAuthentication() 也引用了这个对象，这里只是作示例提醒一下。
                                    log.info("SecurityContextHolder.getContext().getAuthentication() = " + SecurityContextHolder.getContext().getAuthentication());
                                    log.info("authentication = " + authentication);
                                    response.sendRedirect("/");
                                })
        ;
        // @formatter:on
    }
}
~~~

#### 上面的配置代码注意下面三个步骤：
1. 需要在一个配置类上添加 @EnableSaiOAuth2Login 注解 , 以激活配置；
2. 在安全过滤链配置器中，调用 http.apply(new SaiOAuth2LoginSecurityConfigurer()) ；
3. 可以继续自定义配置 oauth2Login() ,如上面的例子中，自定义了登陆入口的path为 /login 。

### 欢迎大家交流学习
微信：13026844888
QQ：2231068

